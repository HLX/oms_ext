// ==UserScript==
// @name        HLX Test Action
// @namespace   Violentmonkey Scripts
// @match       https://eq.cwa.sellercloud.com/Inventory/*
// @grant       GM_xmlhttpRequest
// @version     0.2
// @author      HLX Studios
// @description 06/28/2021
// ==/UserScript==


// Add custom dropdown (new row on table)
const content = document.getElementById("ContentPlaceHolder1_divContentPlaceHolder");
let table = content.getElementsByTagName("table");
table = table[0]
let tbody = table.tBodies[0];
let new_row = table.insertRow(0);

let new_td = document.createElement("td");
new_td.style.textAlign = "right";

let select_list = document.createElement("select");
select_list.id = "hlx_custom_dropdown";
select_list.style.marginLeft = "5px";
select_list.style.marginRight = "5px";
select_list.style.width = "214px";

let test_action = document.createElement("option");
let hlx_title = document.createElement("span");
let go_button = document.createElement("button");
go_button.textContent = "Go";
go_button.type = "button";
go_button.id = "hlx_go_button";
hlx_title.textContent = "HLX Custom Actions";

test_action.value = "test_action";
test_action.textContent = "Test Action";
select_list.append(test_action);
new_td.append(hlx_title);
new_td.append(select_list);
new_td.append(go_button);
new_row.append(new_td);
// End create custom dropdown

// Listent for click on "Go" button
go_button.addEventListener("click", () => {
  const selected_option_value = select_list.value;
  const selected_option_text  = select_list.options[select_list.selectedIndex].text;
  
  if (selected_option_text === "Test Action") {
    log_info();
  }
  
});

const log_info = () => {
  const url_endpoint 	= "https://api.hlx.co/logger"
  const url_method 		= "/log_add"
  const url = "https://api.hlx.co/logger/log_add"
  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  const sku = urlParams.get('Id');
    
  // get clientId, clientName and subscription from script tag
  const scr = document.getElementsByTagName("script");
  let asd = scr[scr.length -1 ].textContent;
  let i = asd.indexOf("delightedNps2.survey");
  let new_string = asd.substring(i,);
  const j = new_string.indexOf("});");
  i = new_string.indexOf("({");
  new_string = new_string.substring(i + 1,j);
  new_string += "}";
  var obj = eval('(' + new_string + ')');
  var json = JSON.stringify(obj);
  const object_data = JSON.parse(json);
  const clientId = object_data.properties.clientId;
  const clientName = object_data.properties.clientName;
  const subscription = object_data.properties.subscription;
  const name = object_data.name;
  const email = object_data.email;
    
  const payload = {
    "ProductID": sku
    , "name": name
    , "email": email
    , "Client ID": clientId
    , "Client Name": clientName
    , "Subscription": subscription
  }
  const message = `Test Action - ${JSON.stringify(payload)}`
  sendApiRequest(url, message)
    .then(d => {
        let alert_row = table.insertRow(0);

        let alert_td = document.createElement("td");
        alert_td.style.textAlign = "right";
    
        if (d.status === "success") {
          //console.log("Success");
          alert_td.textContent = "Success, product received";
        } else {
          //console.log("Could not log this product");
          alert_td.textContent = "Failure, Could not log this product";
          
        }
        alert_row.append(alert_td);
    
    })
  
}


async function sendApiRequest(url, message){
  return new Promise((resolve, reject) => {
    GM_xmlhttpRequest({
      method: "POST",
      url: url,
      headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
      data: "event-message=" + encodeURIComponent(message),      
      onload: function(response) {
        resolve(JSON.parse(response.responseText));
      }
    });
  })  
}
